<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

    
<div class="lazyOwl slide" 
	<?php if ($params->get('backgroundimage')) : ?> data-original='<?php echo $params->get('backgroundimage');?>' 
    style="background-image:url(<?php echo $params->get('backgroundimage');?>)"
	<?php endif;?> 
    >
	<div class="content-slide">
		<?php echo $module->content;?>
</div>
</div>
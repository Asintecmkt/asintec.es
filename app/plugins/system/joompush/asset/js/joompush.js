jQuery( document ).ready(function() {
	var config = {
		apiKey: apiKey,
		authDomain: project_id + ".firebaseapp.com",
		databaseURL: "https://" + project_id + ".firebaseio.com",
		storageBucket: project_id + ".appspot.com",
		messagingSenderId: messagingSenderId,
	};
	firebase.initializeApp(config);
	// Retrieve Firebase Messaging object.
	const messaging = firebase.messaging();
	
	if(getCookie('jpsent') == 0)
	{
		// On load register service worker
		if ('serviceWorker' in navigator) {
		  window.addEventListener('load', () => {
			navigator.serviceWorker.register(sw_url).then((registration) => {
			  // Successfully registers service worker
			  console.log('ServiceWorker registration successful with scope: ', registration.scope);
			  messaging.useServiceWorker(registration);
			})
			.then(() => {
			  // Requests user browser permission
			  return messaging.requestPermission();
			})
			.then(() => {
			  // Gets token
			  return messaging.getToken();
			})
			.then((token) => {
			  // Simple ajax call to send user token to server for saving
			  var storeurl = baseurl + 'index.php?option=com_joompush&task=mynotifications.setSubscriber';
			  console.log(storeurl + token);
			  jQuery.ajax({
				type: 'post',
				url: storeurl,
				data: {key: token, IsClient: isClient, Userid: userid},
				success: (data) => {
				  console.log('Success ', data);
				  document.cookie = "jpsent = 1;"
				},
				error: (err) => {
				  console.log('Error ', err);
				}
			  })
			})
			.catch((err) => {
			  console.log('ServiceWorker registration failed: ', err);
			});
		  });
		}
	}
});


	
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

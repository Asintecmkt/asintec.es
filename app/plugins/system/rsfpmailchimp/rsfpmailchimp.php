<?php
/**
 * @package        RSForm! Pro
 * @copyright  (c) 2007 - 2016 RSJoomla!
 * @link           https://www.rsjoomla.com
 * @license        GNU General Public License http://www.gnu.org/licenses/gpl-3.0.en.html
 */

defined('_JEXEC') or die('Restricted access');

/**
 * Class plgSystemRSFPMailChimp
 */
class plgSystemRSFPMailChimp extends JPlugin
{
	/**
	 * @var bool
	 */
	protected $autoloadLanguage = true;

	/**
	 * Save the form properties
	 *
	 * @param $form
	 *
	 * @return bool|void
	 */
	public function rsfp_onFormSave($form)
	{
		$application          = JFactory::getApplication();
		$jinput               = $application->input;
		$postArray            = $jinput->getArray($_POST);
		$postArray['form_id'] = $postArray['formId'];

		$row = JTable::getInstance('RSForm_MailChimp', 'Table');
		if (!$row)
		{
			return false;
		}

		try
		{
			if (!$row->bind($postArray))
			{
				throw new Exception (JText::_('COM_RSFP_MAILCHIMP_COULD_NOT_BIND_DATA'));
			}

			$row->mc_merge_vars      = serialize($postArray['merge_vars']);
			$row->mc_cached_values   = serialize($postArray['mc_cached_values']);
			$row->mc_interest_groups = serialize($postArray['interest_groups']);

			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('form_id')
				->from($db->qn('#__rsform_mailchimp'))
				->where($db->qn('form_id') . '=' . $db->q((int) $postArray['form_id']));
			$db->setQuery($query);
			if (!$db->loadResult())
			{
				$query->clear();
				$query->insert('#__rsform_mailchimp')
					->set(array(
						$db->qn('form_id') . '=' . $db->q($form->FormId),
					));
				$db->setQuery($query)->execute();
			}

			if (!$row->store())
			{
				throw new Exception (JText::_('COM_RSFP_MAILCHIMP_COULD_NOT_STORE_DATA'));
			}

			return true;
		} catch (Exception $e)
		{
			$application->enqueueMessage($e->getMessage(), 'error');

			return false;
		}

	}

	/**
	 * Function that generates the Form Properties - Mailchimp tab content
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function rsfp_bk_onAfterShowFormEditTabs()
	{
		$formId = JFactory::getApplication()->input->getInt('formId', 0);
		$row    = JTable::getInstance('RSForm_MailChimp', 'Table');
		$app    = JFactory::getApplication();

		if (!$row)
		{
			return false;
		}

		$row->load($formId);
		$row->mc_merge_vars = @unserialize($row->mc_merge_vars);

		if ($row->mc_merge_vars === false)
		{
			$row->mc_merge_vars = array();
		}

		$row->mc_interest_groups = @unserialize($row->mc_interest_groups);
		if ($row->mc_interest_groups === false)
		{
			$row->mc_interest_groups = array();
		}

		require_once JPATH_ADMINISTRATOR . '/components/com_rsform/helpers/mcapi.php';
		$mailchimp = RSFormPro_Mailchimp::getInstance();

		// Fields
		$fields_array = $this->_getFields($formId);
		$fields       = array();
		foreach ($fields_array as $field)
		{
			$fields[] = JHtml::_('select.option', $field, $field);
		}

		// Action
		$mailchimp_action   = array(
			JHtml::_('select.option', 1, JText::_('RSFP_MAILCHIMP_ACTION_SUBSCRIBE')),
			JHtml::_('select.option', 0, JText::_('RSFP_MAILCHIMP_ACTION_UNSUBSCRIBE')),
			JHtml::_('select.option', 2, JText::_('RSFP_MAILCHIMP_LET_USER_DECIDE'))
		);
		$lists['mc_action'] = JHtml::_('select.genericlist', $mailchimp_action, 'mc_action', 'onchange="rsfp_changeMcAction(this);"', 'value', 'text', $row->mc_action);

		// Action Field
		$lists['mc_action_field'] = JHtml::_('select.genericlist', $fields, 'mc_action_field', $row->mc_action != 2 ? 'disabled="disabled"' : '', 'value', 'text', $row->mc_action_field);

		// Email Type
		$mailchimp_email_type   = array(
			JHtml::_('select.option', 'html', JText::_('RSFP_MAILCHIMP_HTML')),
			JHtml::_('select.option', 'text', JText::_('RSFP_MAILCHIMP_TEXT')),
			JHtml::_('select.option', 'mobile', JText::_('RSFP_MAILCHIMP_MOBILE')),
			JHtml::_('select.option', 'user', JText::_('RSFP_MAILCHIMP_LET_USER_DECIDE'))
		);
		$lists['mc_email_type'] = JHtml::_('select.genericlist', $mailchimp_email_type, 'mc_email_type', 'onchange="rsfp_changeMcEmailType(this);"', 'value', 'text', $row->mc_email_type);

		// Email Type Field
		$lists['mc_email_type_field'] = JHtml::_('select.genericlist', $fields, 'mc_email_type_field', $row->mc_email_type != 'user' ? 'disabled="disabled"' : '', 'value', 'text', $row->mc_email_type_field);

		// MailChimp Lists
		$results = $mailchimp->get_lists();

		$mailchimp_lists = array(
			JHtml::_('select.option', '', JText::_('RSFP_PLEASE_SELECT_LIST'))
		);

		$lists['mc_list_id'] = '';
		if ($results)
		{
			foreach ($results as $result)
			{
				$mailchimp_lists[]   = JHtml::_('select.option', $result['id'], $result['name']);
				$lists['mc_list_id'] = JHtml::_('select.genericlist', $mailchimp_lists, 'mc_list_id', 'onchange="rsfp_changeMcList(this);"', 'value', 'text', $row->mc_list_id);
			}
		}

		// Merge Vars
		$merge_vars = JText::_('RSFP_PLEASE_SELECT_LIST');
		if ($row->mc_list_id)
		{
			$merge_vars = $mailchimp->get_merge_fields($row->mc_list_id);
		}
		
		$lists['fields'] = array();
		if (is_array($merge_vars))
		{
			foreach ($merge_vars as $merge_var)
			{
				$value = isset($row->mc_merge_vars[$merge_var->tag]) ? $row->mc_merge_vars[$merge_var->tag] : null;

				$lists['fields'][$merge_var->tag] = '<input id="' . $merge_var->tag . '" data-delimiter=" " data-filter-type="include" data-filter="value,path,localpath,text,global" data-placeholders="display" size="100" maxlength="64" name="merge_vars[' . $merge_var->tag . ']" value="' . $value . '" type="text">';
			}
		}

		// Interest Groups
		$interest_groups = JText::_('RSFP_PLEASE_SELECT_LIST');
		if ($row->mc_list_id)
		{
			$interest_groups = $mailchimp->get_interest_categories($row->mc_list_id);
		}

		$lists['fields_groups'] = array();
		if (is_array($interest_groups))
		{
			foreach ($interest_groups as $interest_group)
			{
				$lists['fields_groups'][$interest_group->id] = JHtml::_('select.genericlist', $fields, 'interest_groups[' . $interest_group->title . ']', null, 'value', 'text', isset($row->mc_interest_groups[$interest_group->title]) ? $row->mc_interest_groups[$interest_group->title] : null);

				$lists['field_groups_desc'][$interest_group->id] = array();
				
				$interests = $mailchimp->get_interests($row->mc_list_id, $interest_group->id);
				$cached_values = array();
				if ($interests)
				{
					foreach ($interests as $group)
					{
						$lists['field_groups_desc'][$interest_group->id][] = $group->id . '|' . $group->name;
						$cached_values[$group->name] = $group->id;
					}
				}
				
				$lists['field_groups_desc'][$interest_group->id] = implode("\n", $lists['field_groups_desc'][$interest_group->id]);
				$lists['cached_values'][$interest_group->id] = '<input type="hidden" name="mc_cached_values['.htmlspecialchars($interest_group->title).']" value="'.htmlspecialchars(json_encode($cached_values)).'" />';
			}
		}

		$lists['mc_double_optin']      = RSFormProHelper::renderHTML('select.booleanlist', 'mc_double_optin', 'class="inputbox"', $row->mc_double_optin);
		$lists['mc_update_existing']   = RSFormProHelper::renderHTML('select.booleanlist', 'mc_update_existing', 'class="inputbox"', $row->mc_update_existing);
		$lists['mc_replace_interests'] = RSFormProHelper::renderHTML('select.booleanlist', 'mc_replace_interests', 'class="inputbox"', $row->mc_replace_interests);

		$lists['mc_delete_member'] = RSFormProHelper::renderHTML('select.booleanlist', 'mc_delete_member', 'class="inputbox"', $row->mc_delete_member);

		$lists['published'] = RSFormProHelper::renderHTML('select.booleanlist', 'mc_published', 'class="inputbox"', $row->mc_published);

		include JPATH_ADMINISTRATOR . '/components/com_rsform/helpers/mailchimp.php';

		return true;
	}

	/**
	 * Button from the Form Properties Menu ( LEFT SIDE )
	 */
	public function rsfp_bk_onAfterShowFormEditTabsTab()
	{
		echo '<li><a href="javascript: void(0);" id="mailchimp"><span class="rsficon rsficon-envelope-o"></span><span class="inner-text">' . JText::_('RSFP_MAILCHIMP_INTEGRATION') . '</span></a></li>';
	}

	/**
	 * Hook in the RSForm!Pro submission storing process.
	 *
	 * @param $args
	 *
	 * @return bool
	 */
	public function rsfp_f_onBeforeStoreSubmissions($args)
	{
		$formId       = (int) $args['formId'];
		$SubmissionId = (int) $args['SubmissionId'];

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from($db->qn('#__rsform_mailchimp'))
			->where($db->qn('form_id') . '=' . $db->q((int) $formId))
			->where($db->qn('mc_published') . '=' . $db->q(1));
		$db->setQuery($query);
		if ($row = $db->loadObject())
		{
			if (!$row->mc_list_id)
			{
				return false;
			}

			require_once JPATH_ADMINISTRATOR . '/components/com_rsform/helpers/mcapi.php';
			$mailchimp = RSFormPro_Mailchimp::getInstance();

			$row->mc_merge_vars = @unserialize($row->mc_merge_vars);
			if ($row->mc_merge_vars === false)
			{
				$row->mc_merge_vars = array();
			}
			$row->mc_cached_values = @unserialize($row->mc_cached_values);
			if ($row->mc_cached_values === false)
			{
				$row->mc_cached_values = array();
			}
			if (is_array($row->mc_cached_values))
			{
				foreach ($row->mc_cached_values as $group => $values)
				{
					$row->mc_cached_values[$group] = json_decode($values, true);
				}
			}
			if (empty($row->mc_merge_vars['EMAIL']))
			{
				return false;
			}

			$row->mc_interest_groups = @unserialize($row->mc_interest_groups);
			if ($row->mc_interest_groups === false)
			{
				$row->mc_interest_groups = array();
			}
			
			$merge_vars = $row->mc_merge_vars;

			$form          = $args['post'];
			$email_address = $row->mc_merge_vars['EMAIL'];

			// Interest Groups
			$merge_interest = array();

			if (!empty($row->mc_interest_groups))
			{
				foreach ($row->mc_interest_groups as $group => $field)
				{
					if ($field == '- IGNORE -' || !isset($form[$field]))
					{
						continue;
					}

					$interests = array();
					if (is_array($form[$field]))
					{
						$interests = array_merge($interests, $form[$field]);
					}
					else
					{
						$interests[] = $form[$field];
					}
					
					if (isset($row->mc_cached_values[$group]))
					{
						foreach ($interests as $key => $value)
						{
							if (isset($row->mc_cached_values[$group][$value]))
							{
								$interests[$key] = $row->mc_cached_values[$group][$value];
							}
						}
					}

					$merge_interest[] = array(
						'name'   => $group,
						'groups' => $interests
					);
				}
			}

			// Email Type
			$email_type = $row->mc_email_type;
			$valid      = array('html', 'text', 'mobile');
			if ($row->mc_email_type == 'user')
			{
				$email_type = isset($form[$row->mc_email_type_field]) && in_array(strtolower(trim($form[$row->mc_email_type_field])), $valid) ? $form[$row->mc_email_type_field] : 'html';
			}

			// Subscribe action - Subscribe, Unsubscribe or Let the user choose
			$subscribe = '';
			if ($row->mc_action == 1)
			{
				$subscribe = 'subscribe';
			}
			elseif ($row->mc_action == 0)
			{
				$subscribe = 'unsubscribe';
			}
			elseif ($row->mc_action == 2 && isset($form[$row->mc_action_field]))
			{
				if (is_array($form[$row->mc_action_field]))
				{
					foreach ($form[$row->mc_action_field] as $i => $value)
					{
						$value = strtolower(trim($value));
						if ($value == 'subscribe')
						{
							$subscribe = 'subscribe';
							break;
						}
						elseif ($value == 'unsubscribe')
						{
							$subscribe = 'unsubscribe';
							break;
						}
					}
				}
				else
				{
					$form[$row->mc_action_field] = strtolower(trim($form[$row->mc_action_field]));
					if ($form[$row->mc_action_field] == 'subscribe')
					{
						$subscribe = 'subscribe';
					}
					elseif ($form[$row->mc_action_field] == 'unsubscribe')
					{
						$subscribe = 'unsubscribe';
					}
				}
			}

			$this->mailchimpData = array(
				'row'               => $row,
				'subscribe'         => $subscribe,
				'list_id'           => $row->mc_list_id,
				'email_address'     => $email_address,
				'merge_vars'        => $merge_vars,
				'email_type'        => $email_type,
				'double_optin'      => $row->mc_double_optin,
				'update_existing'   => $row->mc_update_existing,
				'replace_interests' => $row->mc_replace_interests,
				'delete_member'     => $row->mc_delete_member,
				'interests'         => $merge_interest
			);
		}

		return true;
	}

	/**
	 * Hook after placeholders are created
	 *
	 * @param $args
	 */
	public function rsfp_onAfterCreatePlaceholders($args)
	{
		// Workaround so that uploads are sent to MailChimp
		if (!empty($this->mailchimpData))
		{
			require_once JPATH_ADMINISTRATOR . '/components/com_rsform/helpers/mcapi.php';
			
			$app           	= JFactory::getApplication();
			$mailchimp 		= RSFormPro_Mailchimp::getInstance();
			$mailchimpData 	= $this->mailchimpData;

			// Unset the MailChimp object now that we're done with it.
			$this->mailchimpData = null;

			$data = array(
				'email_address' => str_replace($args['placeholders'], $args['values'], $mailchimpData['email_address']),
				'merge_fields'  => (object) str_replace($args['placeholders'], $args['values'], $mailchimpData['merge_vars']),
				'email_type'    => $mailchimpData['email_type'],
				'interests'     => $mailchimpData['interests'],
			);

			if ($mailchimpData['subscribe'] == 'subscribe' || $mailchimpData['subscribe'] == 'unsubscribe')
			{
				// Subscribing?
				if ($mailchimpData['subscribe'] == 'subscribe')
				{
					$data['status'] = $mailchimpData['double_optin'] ? 'pending' : 'subscribed';
				}
				// Unsubscribing
				else
				{
					$data['status'] = 'unsubscribed';
				}
				
				$mailchimp->manage_user($mailchimpData['list_id'], $mailchimpData['update_existing'], $mailchimpData['replace_interests'], $mailchimpData['delete_member'], $data);
			}
		}
	}

	/**
	 * Function to generate the Tabs
	 *
	 * @param $tabs
	 */
	public function rsfp_bk_onAfterShowConfigurationTabs($tabs)
	{
		$tabs->addTitle(JText::_('MailChimp'), 'form-mailchimp');
		$tabs->addContent($this->mailChimpConfigurationScreen());
	}

	/**
	 * Create the mailchimp configuration screen
	 *
	 * @return string
	 */
	public function mailChimpConfigurationScreen()
	{
		ob_start();

		$lists['mailchimpsecure'] = RSFormProHelper::renderHTML('select.booleanlist', 'rsformConfig[mailchimp.secure]', null, RSFormProHelper::getConfig('mailchimp.secure'));
		?>
		<div id="page-mailchimp" class="com-rsform-css-fix">
			<p><?php echo JText::_('RSFP_MAILCHIMP_API_KEY_INFO'); ?></p>
			<table class="admintable">
				<tr>
					<td width="200" style="width: 200px;" align="right" class="key">
						<label for="mailchimpkey"><span class="hasTip" title="<?php echo JText::_('RSFP_MAILCHIMP_API_KEY_DESC'); ?>"><?php echo JText::_('RSFP_MAILCHIMP_API_KEY'); ?></span></label>
					</td>
					<td>
						<input type="text" name="rsformConfig[mailchimp.key]" id="mailchimpkey" value="<?php echo RSFormProHelper::htmlEscape(RSFormProHelper::getConfig('mailchimp.key')); ?>" size="100" maxlength="100">
					</td>
				</tr>
				<tr>
					<td width="200" style="width: 200px;" align="right" class="key">
						<label for="mailchimpsecure"><span class="hasTip" title="<?php echo JText::_('RSFP_MAILCHIMP_SECURE_DESC'); ?>"><?php echo JText::_('RSFP_MAILCHIMP_SECURE'); ?></span></label>
					</td>
					<td><?php echo $lists['mailchimpsecure']; ?></td>
				</tr>
				<tr>
					<td width="200" style="width: 200px;" align="right" class="key">
						<label for="mailchimplist_count"><span class="hasTip" title="<?php echo JText::_('RSFP_MAILCHIMP_LIST_COUNT_DESC'); ?>"><?php echo JText::_('RSFP_MAILCHIMP_LIST_COUNT'); ?></span></label>
					</td>
					<td>
						<input type="text" name="rsformConfig[mailchimp.list_count]" id="mailchimplist_count" value="<?php echo RSFormProHelper::htmlEscape(RSFormProHelper::getConfig('mailchimp.list_count')); ?>" size="100" maxlength="100">
					</td>
				</tr>
			</table>
		</div>
		<?php

		$contents = ob_get_contents();
		ob_end_clean();

		return $contents;
	}

	/**
	 * Function that handles the AJAX requests
	 *
	 * @throws Exception
	 */
	public function rsfp_bk_onSwitchTasks()
	{
		$input       = JFactory::getApplication()->input;
		$plugin_task = $input->getString('plugin_task', '');

		switch ($plugin_task)
		{
			case 'get_merge_vars':
				require_once JPATH_ADMINISTRATOR . '/components/com_rsform/helpers/mcapi.php';
				if ($list_id = $input->post->get('list_id', '', 'string'))
				{
					echo json_encode(RSFormPro_Mailchimp::getInstance()->get_merge_fields($list_id));
				}
				jexit();
			break;

			case 'get_interest_groups':
				require_once JPATH_ADMINISTRATOR . '/components/com_rsform/helpers/mcapi.php';
				if ($list_id = $input->post->get('list_id', '', 'string'))
				{
					$mailchimp = RSFormPro_Mailchimp::getInstance();
					
					$response = array();
					if ($categories = $mailchimp->get_interest_categories($list_id))
					{
						foreach ($categories as $category)
						{
							$interest = array(
								'title' 	=> $category->title,
								'subgroups' => array()
							);
							
							if ($interests = $mailchimp->get_interests($list_id, $category->id))
							{
								foreach ($interests as $subgroup)
								{
									$interest['subgroups'][] = array('title' => $subgroup->name, 'id' => $subgroup->id);
								}
							}
							
							$response[] = $interest;
						}
					}
					echo json_encode($response);
				}
				jexit();
			break;
		}
	}

	/**
	 * Helper function to get the form fields
	 *
	 * @param $formId
	 *
	 * @return mixed
	 */
	protected function _getFields($formId)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true)
			->select($db->qn('p.PropertyValue'))
			->from($db->qn('#__rsform_components', 'c'))
			->join('LEFT', $db->qn('#__rsform_properties', 'p') . ' ON (' . $db->qn('c.ComponentId') . '=' . $db->qn('p.ComponentId') . ')')
			->where($db->qn('c.FormId') . '=' . $db->q($formId))
			->where($db->qn('p.PropertyName') . '=' . $db->q('NAME'))
			->order($db->qn('c.Order') . ' ' . $db->escape('ASC'));

		$fields = $db->setQuery($query)->loadColumn();
		array_unshift($fields, '- IGNORE -');

		return $fields;
	}

	/**
	 * Handle uninstall
	 *
	 * @param $formId
	 */
	public function rsfp_onFormDelete($formId)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->delete('#__rsform_mailchimp')
			->where($db->qn('form_id') . '=' . $db->q($formId));
		$db->setQuery($query)->execute();
	}

	/**
	 * Handle Form Backup
	 *
	 * @param $form
	 * @param $xml
	 * @param $fields
	 */
	public function rsfp_onFormBackup($form, $xml, $fields)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from($db->qn('#__rsform_mailchimp'))
			->where($db->qn('form_id') . '=' . $db->q($form->FormId));
		$db->setQuery($query);
		if ($mailchimp = $db->loadObject())
		{
			// No need for a form_id
			unset($mailchimp->form_id);

			$xml->add('mailchimp');
			foreach ($mailchimp as $property => $value)
			{
				$xml->add($property, $value);
			}
			$xml->add('/mailchimp');
		}
	}

	/**
	 * Handle Form Restores
	 *
	 * @param $form
	 * @param $xml
	 * @param $fields
	 */
	public function rsfp_onFormRestore($form, $xml, $fields)
	{
		if (isset($xml->mailchimp))
		{
			$data = array(
				'form_id' => $form->FormId
			);

			foreach ($xml->mailchimp->children() as $property => $value)
			{
				$data[$property] = (string) $value;
			}

			$row = JTable::getInstance('RSForm_MailChimp', 'Table');

			if (!$row->load($form->FormId))
			{
				$db    = JFactory::getDbo();
				$query = $db->getQuery(true);
				$query->insert('#__rsform_mailchimp')
					->set(array(
						$db->qn('form_id') . '=' . $db->q($form->FormId),
					));
				$db->setQuery($query)->execute();
			}
			
			// Migrate to new version
			if (isset($data['mc_send_welcome']))
			{
				$data['mc_merge_vars'] = @unserialize($data['mc_merge_vars']);
				if (!is_array($data['mc_merge_vars']))
				{
					$data['mc_merge_vars'] = array();
				}
				$new_merge_vars = array();
				foreach ($mc_merge_vars as $merge_var => $field)
				{
					$new_merge_vars[$merge_var] = '';
					if ($field != '- IGNORE -')
					{
						if ($exists = RSFormProHelper::componentNameExists($field, $form->FormId, 0, 'ComponentTypeId'))
						{
							if ($exists[0] == RSFORM_FIELD_FILEUPLOAD)
							{
								$new_merge_vars[$merge_var] = '{' . $field . ':path}';
							}
							else
							{
								$new_merge_vars[$merge_var] = '{' . $field . ':value}';
							}
						}
					}
				}
				
				$data['mc_merge_vars'] = serialize($new_merge_vars);
			}

			$row->save($data);
		}
	}

	/**
	 * Truncate tables on restores
	 */
	public function rsfp_bk_onFormRestoreTruncate()
	{
		JFactory::getDbo()->truncateTable('#__rsform_mailchimp');
	}
}
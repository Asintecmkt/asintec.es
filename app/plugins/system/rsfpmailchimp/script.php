<?php
/**
* @package RSForm!Pro
* @copyright (C) 2007-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class plgSystemRSFPMailChimpInstallerScript
{
	public function preflight($type, $parent) {
		if ($type == 'uninstall') {
			return true;
		}
		
		$app = JFactory::getApplication();
		
		if (!file_exists(JPATH_ADMINISTRATOR.'/components/com_rsform/helpers/rsform.php')) {
			$app->enqueueMessage('Please install the RSForm! Pro component before continuing.', 'error');
			return false;
		}
		
		if (!file_exists(JPATH_ADMINISTRATOR.'/components/com_rsform/helpers/assets.php')) {
			$app->enqueueMessage('Please upgrade RSForm! Pro to at least version 1.51.0 before continuing!', 'error');
			return false;
		}
		
		$jversion = new JVersion();
		if (!$jversion->isCompatible('2.5.28')) {
			$app->enqueueMessage('Please upgrade to at least Joomla! 2.5.28 before continuing!', 'error');
			return false;
		}
		
		return true;
	}
	
	public function update($parent) {
		$this->copyFiles($parent);
		
		require_once JPATH_ADMINISTRATOR . '/components/com_rsform/helpers/rsform.php';
		
		$db = JFactory::getDbo();
		
		// check if the list_count option is added
		if (!RSFormProHelper::getConfig('mailchimp.list_count')) {
			$db->setQuery("INSERT IGNORE INTO `#__rsform_config` (`SettingName`, `SettingValue`) VALUES ('mailchimp.list_count', '10')")->execute();
		}
		
		$columns = $db->getTableColumns('#__rsform_mailchimp');
		if (isset($columns['mc_send_goodbye']))
		{
			$db->setQuery("ALTER TABLE #__rsform_mailchimp DROP mc_send_welcome, DROP mc_send_goodbye, DROP mc_send_notify")->execute();
			$db->setQuery("ALTER TABLE #__rsform_mailchimp ADD mc_cached_values text NOT NULL AFTER `mc_merge_vars`")->execute();
			
			$query = $db->getQuery(true);
			$query->select('*')
				->from($db->qn('#__rsform_mailchimp'))
				->where($db->qn('mc_merge_vars') . ' != ' . $db->q(''));
			if ($results = $db->setQuery($query)->loadObjectList())
			{
				foreach ($results as $result)
				{
					$mc_merge_vars = @unserialize($result->mc_merge_vars);
					if (is_array($mc_merge_vars))
					{
						$new_merge_vars = array();
						foreach ($mc_merge_vars as $merge_var => $field)
						{
							$new_merge_vars[$merge_var] = '';
							
							if ($field != '- IGNORE -')
							{
								if ($exists = RSFormProHelper::componentNameExists($field, $result->form_id, 0, 'ComponentTypeId'))
								{
									if ($exists[0] == RSFORM_FIELD_FILEUPLOAD)
									{
										$new_merge_vars[$merge_var] = '{' . $field . ':path}';
									}
									else
									{
										$new_merge_vars[$merge_var] = '{' . $field . ':value}';
									}
								}
							}
						}
						
						$query->clear()
							->update('#__rsform_mailchimp')
							->set($db->qn('mc_merge_vars') . ' = ' . $db->q(serialize($new_merge_vars)))
							->where($db->qn('form_id') . ' = ' . $db->q($result->form_id));
						
						$db->setQuery($query)->execute();
					}
				}
			}
		}
	}
	
	public function install($parent) {
		$this->copyFiles($parent);
	}
	
	protected function copyFiles($parent) {
		$app = JFactory::getApplication();
		$installer = $parent->getParent();
		$src = $installer->getPath('source').'/admin';
		$dest = JPATH_ADMINISTRATOR.'/components/com_rsform';
		
		if (!JFolder::copy($src, $dest, '', true)) {
			$app->enqueueMessage('Could not copy to '.str_replace(JPATH_SITE, '', $dest).', please make sure destination is writable!', 'error');
		}
	}
}
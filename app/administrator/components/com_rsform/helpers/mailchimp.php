<?php
/**
 * @version       1.3.0
 * @package       RSform!Pro 1.3.0
 * @copyright (C) 2007-2010 www.rsjoomla.com
 * @license       GPL, http://www.gnu.org/copyleft/gpl.html
 */

defined('_JEXEC') or die('Restricted access');
?>
<div id="mailchimpdiv">
	<script type="text/javascript">
		function rsfp_changeMcList(what) {
			var value = jQuery(what).val(),
				params = {list_id: null},
				merge_vars = jQuery('#merge_vars'),
				interest_groups = jQuery('#interest_groups'),
				RSFP_MAILCHIMP_INTEREST_GROUP_DESC = '<?php echo JText::_('RSFP_MAILCHIMP_INTEREST_GROUP_DESC', true); ?>',
				merge_vars_url = 'index.php?option=com_rsform&task=plugin&plugin_task=get_merge_vars',
				interest_groups_url = 'index.php?option=com_rsform&task=plugin&plugin_task=get_interest_groups',
				state = jQuery('#state');

			state.html('Status: loading...').css('color', 'rgb(255,0,0)');
			params.list_id = value;

			jQuery.ajax({
				type    : "POST",
				url     : merge_vars_url,
				data    : params,
				success : function (response) {
					merge_vars.find("tr:gt(0)").remove();
					if (response) {
						jQuery.each(response, function () {
							merge_vars.append('<tr><td>(' + this.tag + ') ' + this.name + '</td><td><input id="' + this.tag + '" name="merge_vars[' + this.name + ']" type="text" data-delimiter=" " data-filter-type="include" data-filter="value,path,localpath,text,global" data-placeholders="display" size="100" maxlength="64" /></td></tr>');
						});
					}

					merge_vars.find('[data-placeholders]:not(#EMAIL)').rsplaceholder();
				},
				dataType: 'json'
			});

			jQuery.ajax({
				type    : "POST",
				url     : interest_groups_url,
				data    : params,
				success : function (response) {
					interest_groups.find("tr").remove();
					if (response) {
						jQuery.each(response, function () {
							interest_groups.append('<tr><td>' + this.title + '</td><td><select name="interest_groups[' + this.title + ']">' +
								<?php foreach ($fields_array as $field) { ?>
								'<option value="<?php echo $field ?>"><?php echo $field ?></option>' +
								<?php } ?>
								'</select></td></tr>');

							var replace = [];
							jQuery.each(this.subgroups, function () {
								var $val = this.id + '|' + this.title;
								replace.push($val);
							});

							interest_groups.append('<tr><td></td><td>' + RSFP_MAILCHIMP_INTEREST_GROUP_DESC.replace('%s', replace.join("\n")) + '</td></tr>');
						});
					}
				},
				dataType: 'json'
			});

			state.html('Status: ok').css('color', '');
		}

		function rsfp_changeMcEmailType(what) {
			jQuery('#mc_email_type_field').prop('disabled', what.value !== 'user');
		}

		function rsfp_changeMcAction(what) {
			jQuery('#mc_action_field').prop('disabled', what.value != 2);
		}
	</script>
	<style>
		#merge_vars input {
			margin-bottom : 0;
		}
	</style>
	<table class="admintable">
		<tr>
			<td valign="top" align="left" width="30%">
				<table class="table table-bordered">
					<tr>
						<td colspan="2" class="center"><?php echo JHtml::image('administrator/components/com_rsform/assets/images/mailchimp.png', 'MailChimp'); ?></td>
					</tr>
					<tr>
						<td colspan="2">
							<div class="alert alert-info"><?php echo JText::_('RSFP_MAILCHIMP_DESC'); ?></div>
						</td>
					</tr>
					<tr>
						<td width="80" align="right" nowrap="nowrap" class="key"><?php echo JText::_('RSFP_MAILCHIMP_USE_INTEGRATION'); ?></td>
						<td><?php echo $lists['published']; ?></td>
					</tr>
					<tr>
						<td width="80" align="right" nowrap="nowrap" class="key">
							<span class="hasTooltip" title="<?php echo JText::_('RSFP_MAILCHIMP_ACTION_DESC'); ?>"><?php echo JText::_('RSFP_MAILCHIMP_ACTION'); ?></span>
						</td>
						<td nowrap="nowrap"><?php echo $lists['mc_action']; ?><?php echo $lists['mc_action_field']; ?></td>
					</tr>
					<tr>
						<td colspan="2"><?php echo JText::_('RSFP_MAILCHIMP_ACTION_WARNING'); ?></td>
					</tr>
					<tr>
						<td width="80" align="right" nowrap="nowrap" class="key"><?php echo JText::_('RSFP_MAILCHIMP_LIST_ID'); ?></td>
						<td nowrap="nowrap"><?php echo $lists['mc_list_id']; ?></td>
					</tr>
					<tr>
						<td width="80" align="right" nowrap="nowrap" class="key">
							<span class="hasTooltip" title="<?php echo JText::_('RSFP_MAILCHIMP_EMAIL_TYPE_DESC'); ?>"><?php echo JText::_('RSFP_MAILCHIMP_EMAIL_TYPE'); ?></span>
						</td>
						<td nowrap="nowrap"><?php echo $lists['mc_email_type']; ?><?php echo $lists['mc_email_type_field']; ?></td>
					</tr>
					<tr>
						<td colspan="2"><?php echo JText::_('RSFP_MAILCHIMP_EMAIL_TYPE_WARNING'); ?></td>
					</tr>
				</table>
				
				<h3 class="rsfp-legend"><?php echo JText::_('RSFP_MAILCHIMP_SUBSCRIBE_OPTIONS'); ?></h3>
				<table class="admintable">
					<tr>
						<td width="200" nowrap="nowrap">
							<span class="hasTooltip" title="<?php echo JText::_('RSFP_MAILCHIMP_DOUBLE_OPTIN_DESC'); ?>"><?php echo JText::_('RSFP_MAILCHIMP_DOUBLE_OPTIN'); ?></span>
						</td>
						<td><?php echo $lists['mc_double_optin']; ?></td>
					</tr>
					<tr>
						<td width="200" nowrap="nowrap">
							<span class="hasTooltip" title="<?php echo JText::_('RSFP_MAILCHIMP_UPDATE_EXISTING_DESC'); ?>"><?php echo JText::_('RSFP_MAILCHIMP_UPDATE_EXISTING'); ?></span>
						</td>
						<td><?php echo $lists['mc_update_existing']; ?></td>
					</tr>
					<tr>
						<td width="200" nowrap="nowrap">
							<span class="hasTooltip" title="<?php echo JText::_('RSFP_MAILCHIMP_REPLACE_INTERESTS_DESC'); ?>"><?php echo JText::_('RSFP_MAILCHIMP_REPLACE_INTERESTS'); ?></span>
						</td>
						<td><?php echo $lists['mc_replace_interests']; ?></td>
					</tr>
				</table>
				
				<h3 class="rsfp-legend"><?php echo JText::_('RSFP_MAILCHIMP_UNSUBSCRIBE_OPTIONS'); ?></h3>
				<table class="admintable">
					<tr>
						<td width="200" nowrap="nowrap">
							<span class="hasTooltip" title="<?php echo JText::_('RSFP_MAILCHIMP_DELETE_MEMBER_DESC'); ?>"><?php echo JText::_('RSFP_MAILCHIMP_DELETE_MEMBER'); ?></span>
						</td>
						<td><?php echo $lists['mc_delete_member']; ?></td>
					</tr>
				</table>
				
				<h3 class="rsfp-legend"><?php echo JText::_('RSFP_MAILCHIMP_MERGE_VARS'); ?></h3>
				<table class="table table-bordered">
					<tr>
						<td colspan="2"><?php echo JText::_('RSFP_MAILCHIMP_MERGE_VARS_DESC'); ?></td>
					</tr>
					<tbody id="merge_vars">
					<tr>
						<td nowrap="nowrap" align="right">(EMAIL) Email</td>
						<td><input id="EMAIL" data-delimiter=" " data-filter-type="include" data-filter="value,path,localpath,text,global" data-placeholders="display" size="100" maxlength="64" name="merge_vars[EMAIL]" value="<?php echo isset($row->mc_merge_vars['EMAIL']) ? $row->mc_merge_vars['EMAIL'] : null;?>" type="text"></td>
					</tr>
					<?php
					if (is_array($merge_vars))
					{
						foreach ($merge_vars as $merge_var)
						{
							?>
							<tr>
								<td nowrap="nowrap" align="right">(<?php echo $merge_var->tag; ?>) <?php echo $merge_var->name; ?></td>
								<td><?php echo $lists['fields'][$merge_var->tag]; ?></td>
							</tr>
							<?php
						}
					}
					?>
					</tbody>
				</table>
				<h3 class="rsfp-legend"><?php echo JText::_('RSFP_MAILCHIMP_INTERESTS'); ?></h3>
				<table class="table table-bordered">
					<tr>
						<td colspan="2"><?php echo JText::_('RSFP_MAILCHIMP_INTERESTS_DESC'); ?></td>
					</tr>
					<tbody id="interest_groups">
					<?php
					if (is_array($interest_groups))
					{
						foreach ($interest_groups as $interest_group)
						{
							?>
							<tr>
								<td nowrap="nowrap" align="right"><?php echo $interest_group->title; ?></td>
								<td><?php echo $lists['fields_groups'][$interest_group->id]; ?></td>
							</tr>
							<tr>
								<td colspan="2"><?php echo JText::sprintf('RSFP_MAILCHIMP_INTEREST_GROUP_DESC', $lists['field_groups_desc'][$interest_group->id]); ?><?php echo $lists['cached_values'][$interest_group->id]; ?></td>
							</tr>
							<?php
						}
					}
					?>
					</tbody>
				</table>
			</td>
			<td valign="top">
				&nbsp;
			</td>
		</tr>
	</table>
</div>
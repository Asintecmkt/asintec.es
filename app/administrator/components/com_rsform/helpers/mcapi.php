<?php
/**
 * @package        RSForm! Pro
 * @copyright  (c) 2007 - 2018 RSJoomla!
 * @link           https://www.rsjoomla.com
 * @license        GNU General Public License http://www.gnu.org/licenses/gpl-3.0.en.html
 */

/**
 * Class RSFormPro_Mailchimp
 */
class RSFormPro_Mailchimp
{
	/**
	 * The URL from where we will access data.
	 */
	const ENDPOINT = '%s://%s.api.mailchimp.com/3.0/%s';
	/**
	 * This holds the server where the data is located.
	 *
	 * @var $server
	 */
	protected $server;
	/**
	 * The mailchimp API Key
	 *
	 * @var $key
	 */
	protected $key;

	/**
	 * The count option used in retrieving lists (the MailChimp default is 10)
	 *
	 * @var $key
	 */
	protected $count;

	/**
	 * RSFormPro_Mailchimp constructor.
	 */
	public function __construct()
	{
		/**
		 * Sets the mailchimp api key
		 */
		$key = RSFormProHelper::getConfig('mailchimp.key');

		if ($key && strpos($key, '-') !== false)
		{
			// This seems to be a valid key
			$this->key = $key;
			list($api, $this->server) = explode('-', $key);

			// Get the count for lists
			$this->count = (!RSFormProHelper::getConfig('mailchimp.list_count') ? 10 : (int) RSFormProHelper::getConfig('mailchimp.list_count'));
			// this setting must be at least 1
			if (!$this->count) {
				$this->count = 1;
			}
		}
	}

	/**
	 * Returns the instance of the mailchimp object
	 *
	 * @return RSFormPro_Mailchimp
	 */
	public static function getInstance()
	{
		static $inst;
		if (!$inst)
		{
			$inst = new RSFormPro_Mailchimp;
		}

		return $inst;
	}

	/**
	 * Create a request to the mailchimp API
	 *
	 * @param $args
	 *
	 * @return mixed
	 * @throws Exception
	 */
	private function request($args)
	{
		if (!$this->key || !$this->server)
		{
			throw new Exception (JText::_('RSFP_MAILCHIMP_NO_KEY_ADDED'));
		}
		
		$headers = array(
			'Authorization' => 'apikey ' . $this->key
		);

		$http = JHttpFactory::getHttp();

		$params = implode('/', $args['params']);
		
		if (isset($args['q_params']) && !empty($args['q_params'])) {
			$q_params = implode('&', $args['q_params']);
			// Append to the $params
			$params .= '?' . $q_params; 
		}

		$scheme = RSFormProHelper::getConfig('mailchimp.secure') ? 'https' : 'http';
		
		$url = sprintf(self::ENDPOINT, $scheme, $this->server, $params);
		
		$response = null;

		switch ($args['method'])
		{
			case 'get':
				$response = $http->get($url, $headers, 5);
				break;

			case 'post':
				$response = $http->post($url, json_encode($args['data']), $headers, 5);
				break;

			case 'put':
				$headers[]                         = 'X-HTTP-Method-Override';
				$headers['X-HTTP-Method-Override'] = 'PUT';
				$response                          = $http->post($url, json_encode($args['data']), $headers, 5);
				break;

			case 'delete':
				$headers['X-HTTP-Method-Override'] = 'DELETE';
				$response                          = $http->post($url, json_encode($args['data']), $headers, 5);
				break;
		}

		$data = json_decode($response->body);
		if ($data === null)
		{
			throw new Exception (JText::_('RSFP_MAILCHIMP_COULD_NOT_DECODE_JSON_DATA'));
		}
		
		if ($response->code >= 400)
		{
			throw new Exception($data->detail);
		}

		return json_decode($response->body);
	}

	/**
	 * Get the lists
	 *
	 * @return array
	 */
	public function get_lists()
	{
		$args = array(
			'method'   => 'get',
			'params'   => array('lists'),
			'q_params' => array('count='.$this->count)
		);
		try
		{
			$return = array();
			$lists  = $this->request($args);

			if (!empty($lists->lists))
			{
				foreach ($lists->lists as $list)
				{
					$return[] = array(
						'name'  => $list->name,
						'id'    => $list->id,
						'stats' => $list->stats->member_count
					);
				}
			}

			return $return;

		}
		catch (Exception $e)
		{
			JFactory::getApplication()->enqueueMessage('(MailChimp) ' . $e->getMessage(), 'warning');
			return array();
		}
	}

	/**
	 * Adds a user to a certain list
	 *
	 * @param $list_id
	 * @param $update
	 * @param $data
	 * @param $replace
	 * @param $delete
	 *
	 * @return array|string
	 */
	public function manage_user($list_id, $update = false, $replace = false, $delete = false, $data = array())
	{
		$interests     = array();
		$all_interests = array();

		foreach ($data['interests'] as $interest)
		{
			foreach ($interest['groups'] as $subgroup)
			{
				$interests[$subgroup] = true;
			}
		}

		if ($replace)
		{
			$all_interests = $this->create_interests_array($list_id);
		}

		$data['interests'] = array_merge($all_interests, $interests);
		$data['ip_signup'] = JFactory::getApplication()->input->server->getString('REMOTE_ADDR');
		
		// Cast as object apparently
		$data['interests'] = (object) $data['interests'];

		$args = array(
			'method' => 'post',
			'params' => array('lists', $list_id, 'members'),
			'data'   => $data,
		);

		try
		{
			if ((bool) $update)
			{
				$args['method']   = 'put';
				$args['params'][] = md5(strtolower($data['email_address']));
			}

			if ((bool) $delete && $data['status'] == 'unsubscribed')
			{
				$args['method']   = 'delete';
			}

			return $this->request($args);
		}
		catch (Exception $e)
		{
			JFactory::getApplication()->enqueueMessage('(MailChimp) ' . $e->getMessage(), 'warning');
		}
	}

	/**
	 * Creates an array with all the interests assigned to the list id.
	 *
	 * @param $list_id
	 *
	 * @return array
	 */
	public function create_interests_array($list_id)
	{
		$all_interests = array();
		
		try
		{			
			if ($categories = $this->get_interest_categories($list_id))
			{
				foreach ($categories as $category)
				{
					if ($results = $this->get_interests($list_id, $category->id))
					{
						foreach ($results as $interest)
						{
							$all_interests[$interest->id] = false;
						}
					}
				}
			}
		}
		catch (Exception $e)
		{
			JFactory::getApplication()->enqueueMessage('(MailChimp) ' . $e->getMessage(), 'warning');
		}
		
		return $all_interests;
	}
	
	public function get_interests($list_id, $category_id)
	{
		$params = array(
			'method' => 'get',
			'params' => array('lists', $list_id, 'interest-categories', $category_id, 'interests'),
			'q_params' => array('count='.$this->count)
		);
		
		try
		{
			if ($result = $this->request($params))
			{
				if (!empty($result->interests))
				{
					return $result->interests;
				}
			}
		}
		catch (Exception $e)
		{
			JFactory::getApplication()->enqueueMessage('(MailChimp) ' . $e->getMessage(), 'warning');
		}
		
		return false;
	}
	
	public function get_interest_categories($list_id)
	{
		$params = array(
			'method' => 'get',
			'params' => array('lists', $list_id, 'interest-categories'),
			'q_params' => array('count='.$this->count)
		);
		
		try
		{			
			if ($response = $this->request($params))
			{
				return $response->categories;
			}
		}
		catch (Exception $e)
		{
			JFactory::getApplication()->enqueueMessage('(MailChimp) ' . $e->getMessage(), 'warning');
		}
		
		return false;
	}
	
	public function get_merge_fields($list_id)
	{
		$params = array(
			'method' => 'get',
			'params' => array('lists', $list_id, 'merge-fields'),
			'q_params' => array('count='.$this->count)
		);

		try
		{
			if ($result = $this->request($params))
			{
				if (!empty($result->merge_fields))
				{
					return $result->merge_fields;
				}
			}
		}
		catch (Exception $e)
		{
			JFactory::getApplication()->enqueueMessage('(MailChimp) ' . $e->getMessage(), 'warning');
		}
		
		return false;
	}
}
<?php
/**
* @version 1.3.0
* @package RSform!Pro 1.3.0
* @copyright (C) 2007-2010 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/

defined('_JEXEC') or die('Restricted access');

/**
 * Class TableRSForm_MailChimp
 */
class TableRSForm_MailChimp extends JTable
{
	/**
	 * Primary Key
	 *
	 * @public int
	 */
	public $form_id = null;
	/**
	 * @public string
	 */
	public $mc_list_id = '';
	/**
	 * @public int
	 */
	public $mc_action = 1;
	/**
	 * @public string
	 */
	public $mc_action_field = '';
	/**
	 * @public string
	 */
	public $mc_merge_vars = '';
	/**
	 * @public string
	 */
	public $mc_cached_values = '';
	/**
	 * @public string
	 */
	public $mc_interest_groups = '';
	/**
	 * @public string
	 */
	public $mc_email_type = 'html';
	/**
	 * @public string
	 */
	public $mc_email_type_field = '';
	/**
	 * @public int
	 */
	public $mc_double_optin = 1;
	/**
	 * @public int
	 */
	public $mc_update_existing = 0;
	/**
	 * @public int
	 */
	public $mc_replace_interests = 1;
	/**
	 * @public int
	 */
	public $mc_delete_member = 0;

	/**
	 * @public int
	 */
	public $mc_published = 0;

	/**
	 * TableRSForm_MailChimp constructor.
	 *
	 * @param $db
	 */
	public function __construct(& $db)
	{
		parent::__construct('#__rsform_mailchimp', 'form_id', $db);
	}
}
var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var browserSync = require('browser-sync');
var del = require('del');
var php = require('gulp-connect-php7');
var eventStream = require('event-stream');
var runSequence = require('run-sequence');

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

var paths = {
    virtualPathName: "app/templates/asintec/"  
};

function lint(files, options) {
    return () => {
        return gulp.src(files)
            .pipe(reload({ stream: true, once: true }))
            //.pipe($.jshint());
    };
}

function errorHandler(err) {
    $.util.log(
        $.util.colors.red('Unhandled Error - ') + $.util.colors.cyan(err.plugin + '\n'),
        err.toString()
    );
    this.emit('end');
} lint

gulp.task('watch', function () {
    console.log('watch')
    gulp.watch('app/templates/asintec/styles/scss/*.scss', ['styles']);
});

gulp.task('lint', lint('app/templates/asintec/scripts/template.js'));

gulp.task('styles', () => {
    return gulp.src('app/templates/asintec/styles/scss/site.scss')
        .pipe($.sourcemaps.init())
        .pipe($.sass({ precision: 10}).on('error', $.sass.logError))
        .pipe($.autoprefixer({ browsers: ['last 5 version'] }))
        .pipe($.sourcemaps.write('./', { includeContent: false, sourceRoot: '/src' }))
        .pipe(gulp.dest('app/templates/asintec/styles'))
        .pipe($.size({ showFiles: true }));        
});

gulp.task('min', ["min:assets", "min:images"]);

gulp.task('min:assets', function () {
    const assets = $.useref.assets({ searchPath: ['app', '.'] });
    return gulp.src('app/*.html')
        .pipe(assets)
        .pipe($.if('*.js', $.uglify()))
        .pipe($.if('*.css', $.minifyCss({ compatibility: '*' })))
        .pipe(assets.restore())
        .pipe($.useref())
        .pipe($.if('*.html', $.minifyHtml({ conditionals: true, loose: true })))
        .pipe($.size({ showFiles: true }))
        .pipe(gulp.dest('build'));
});

gulp.task('min:images', function () {
    return gulp.src('app/templates/asintec/img/**/*', { base: 'app/templates/asintec/img' })
    .pipe(
        $.if(
            $.if.isFile,
            $.cache(
                $.imagemin({
                    progressive: true,
                    interlaced: true,
                    // don't remove IDs from SVGs, they are often used as hooks for embedding and styling
                    svgoPlugins: [{ cleanupIDs: false }]
                })
            )
        )
        )
        .pipe($.size({ showFiles: true }))
        .pipe(gulp.dest('build/templates/asintec/img'));
});


gulp.task('extras', () => {
    return gulp.src([
        'app/*.*',
        'app/templates/asintec/fonts/*.*',
        'app/templates/asintec/images/**/*',
        '!app/*.html'
    ], {
            dot: true,
            base: 'app'
        })
        .pipe($.size({ showFiles: true }))
        .pipe(gulp.dest('build'));
});

gulp.task('clean', del.bind(null, ['build']));

gulp.task('php:app', function() {
    php.server({ base: 'app', port: 8013, keepalive: true});
});

gulp.task('php:build', function() {
    php.server({ base: 'build', port: 8011, keepalive: true});
});

gulp.task('build',function(done) {
    runSequence('styles','lint', function () {
        done();
    });
});
/*
gulp.task('serve', ['build','watch'], function(done) {
    browserSync({
        port: 9000,
        notify: true,
        server: {
			baseDir: ['app'],
			routes: {

			}
		}
    });
*/
gulp.task('serve', ['build','watch'], (done) => {
    browserSync({
        port: 9000,
        notify: true,
        proxy: '127.0.0.1:80/asintec.io/app/',
    });
    gulp.watch([
        'app/templates/asintec/**/*.php',
        'app/templates/asintec/**/*.html',
        'app/templates/asintec/scripts/**/*.js',
        'app/templates/asintec/styles/**/*.css',
        'app/templates/asintec/images/**/*',
        'app/templates/asintec/fonts/**/*'
    ]).on('change', reload);
    gulp.watch('app/templates/asintec/styles/scss/**/*', function () {
        gulp.run('styles');
    });
});

gulp.task('serve:deploy', ['deploy','php:build'],function(done) {
    browserSync({
        notify: false,
	    proxy: '127.0.0.1:8011',
        port: 9001,
        server: {
            baseDir: ['build']
        }
    }, done);
});


gulp.task('deploy', ['clean'], (done) => {
    runSequence('build', ['min', 'extras'], function () {
        done();
    });
});